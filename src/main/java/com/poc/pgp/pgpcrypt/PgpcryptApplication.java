package com.poc.pgp.pgpcrypt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PgpcryptApplication {

	public static void main(String[] args) {
		SpringApplication.run(PgpcryptApplication.class, args);
	}

}
