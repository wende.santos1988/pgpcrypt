package com.poc.pgp.pgpcrypt.cryptography;

import com.poc.pgp.pgpcrypt.cryptography.generator.KeyGenerators;
import com.poc.pgp.pgpcrypt.utils.LoadFileKeys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

@Component
public class Decryption {

    private static final String ALGORITHM = "RSA";

    @Autowired
    private final KeyGenerators keyGenerators;

    public Decryption(KeyGenerators keyGenerators) {
        this.keyGenerators = keyGenerators;
    }

    public String decrypt(byte[] bytesToDecrypt) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] decryptedText = null;
        PrivateKey privateKey = keyGenerators.getKeyPrivateFromFile(LoadFileKeys.PATH_PRIVATE_KEY);
        try {
            final Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.DECRYPT_MODE, privateKey);
            decryptedText = cipher.doFinal(bytesToDecrypt);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new String(decryptedText);
    }
}
