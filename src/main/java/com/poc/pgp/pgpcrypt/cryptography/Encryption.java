package com.poc.pgp.pgpcrypt.cryptography;

import com.poc.pgp.pgpcrypt.cryptography.generator.KeyGenerators;
import com.poc.pgp.pgpcrypt.utils.LoadFileKeys;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.crypto.Cipher;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;

@Component
public class Encryption {

    private static final String ALGORITHM = "RSA";

    @Autowired
    private final KeyGenerators keyGenerators;

    public Encryption(KeyGenerators keyGenerators) {
        this.keyGenerators = keyGenerators;
    }

    public byte[] encrypt(String fileToEncrypt) throws IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        byte[] encryptedText = null;
        PublicKey publicKey = keyGenerators.getKeyPublicFromFile(LoadFileKeys.PATH_PUBLIC_KEY);
        try {
            final Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE, publicKey);
            encryptedText = cipher.doFinal(fileToEncrypt.getBytes());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return encryptedText;
    }
}
