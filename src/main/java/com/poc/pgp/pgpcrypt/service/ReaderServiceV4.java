package com.poc.pgp.pgpcrypt.service;

import com.poc.pgp.pgpcrypt.cryptography.Decryption;
import com.poc.pgp.pgpcrypt.cryptography.Encryption;
import com.poc.pgp.pgpcrypt.utils.LoadFiles;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Slf4j
public class ReaderServiceV4 {

    private final LoadFiles loadFiles;
    private final Encryption encryption;
    private final Decryption decryption;

    public ReaderServiceV4(LoadFiles loadFiles, Encryption encryption, Decryption decryption) {
        this.loadFiles = loadFiles;
        this.encryption = encryption;
        this.decryption = decryption;
    }

    public void readFileDer() throws Exception {
        log.info("class=ReaderService, method=readFileDer, process=read file.");

        Stream<String> fileToEncrypted = loadFiles.getFileToEncrypted();
        List<String> stringList = fileToEncrypted.collect(Collectors.toList());
        String fileInStringFormatted = formatStringToCrypt(stringList);
        byte[] fileEncrypted = encryption.encrypt(fileInStringFormatted);
        log.info("fileEncrypted={}", fileEncrypted);
        String fileDecrypted = decryption.decrypt(fileEncrypted);
        log.info("fileDecrypted={}", fileDecrypted);
    }

    public void readFileGpg() throws FileNotFoundException {
        log.info("class=ReaderService, method=readFileGpg, process=read file.");
        Stream<String> fileToEncrypted = loadFiles.getFileToEncrypted();
        List<String> stringList = fileToEncrypted.collect(Collectors.toList());
        String fileInStringFormatted = formatStringToCrypt(stringList);
    }

    private String formatStringToCrypt(List<String> file) {
        StringBuilder fileContent = new StringBuilder();
        for (String line : file) {
            fileContent.append(line);
        }
        log.info("content formatted={}", fileContent);
        return fileContent.toString();
    }

}
