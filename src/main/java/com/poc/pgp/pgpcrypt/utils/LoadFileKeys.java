package com.poc.pgp.pgpcrypt.utils;

import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@Service
public class LoadFileKeys {

    private static final String PREFIX_PUBLIC_KEY = "-----BEGIN PGP PUBLIC KEY BLOCK-----";
    private static final String SUFFIX_PUBLIC_KEY = "-----END PGP PUBLIC KEY BLOCK-----";

    private static final String PREFIX_PRIVATE_KEY = "-----BEGIN PGP PRIVATE KEY BLOCK-----";
    private static final String SUFFIX_PRIVATE_KEY = "-----END PGP PRIVATE KEY BLOCK-----";

    public static final String PUBLIC_KEY_PATH = "src/main/resources/public.key";
    public static final String PRIVATE_KEY_PATH = "src/main/resources/private.key";

    public static final String PATH_PUBLIC_KEY = "src/main/resources/keys/public_key.der";
    public static final String PATH_PRIVATE_KEY = "src/main/resources/keys/private_key.der";

    public static final String PATH_PUBLIC_KEY_GPG = "src/main/resources/keys/gpg/publicKey.gpg";
    public static final String PATH_PRIVATE_KEY_GPG = "src/main/resources/keys/gpg/privateKey.gpg";

    public String getPublicKeyPGP() throws IOException {
        //read public key
//        return readFile(PUBLIC_KEY_PATH).replace(PREFIX_PUBLIC_KEY, "").replace(SUFFIX_PUBLIC_KEY, "");
        return readFile(PUBLIC_KEY_PATH);
    }

    public String getPrivateKeyPGP() throws IOException {
        //read private key
//        return readFile(PRIVATE_KEY_PATH).replace(PREFIX_PRIVATE_KEY, "").replace(SUFFIX_PRIVATE_KEY, "");
        return readFile(PRIVATE_KEY_PATH);
    }

    private String readFile(String fileLocation) throws IOException {
        Path path = Paths.get(fileLocation);
        StringBuilder pgpKey = new StringBuilder();
        List<String> lines = Files.readAllLines(path);
        for(String l : lines) {
            pgpKey.append(l);
        }
        return pgpKey.toString();
    }

    public byte[] readFileBytes(String path) throws IOException {
        Path location = Paths.get(path);
        return Files.readAllBytes(location);
    }

}
