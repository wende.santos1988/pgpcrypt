package com.poc.pgp.pgpcrypt.utils;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.stream.Stream;

@Service
public class LoadFiles {

    public Stream<String> getFileToEncrypted() throws FileNotFoundException {
        FileReader fileReader = new FileReader("src/main/resources/test.txt");
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        return bufferedReader.lines();
    }

}
