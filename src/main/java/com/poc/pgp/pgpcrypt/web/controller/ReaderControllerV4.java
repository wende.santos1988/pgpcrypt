package com.poc.pgp.pgpcrypt.web.controller;

import com.poc.pgp.pgpcrypt.service.ReaderServiceV4;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping("/v4/pgp")
@RestController
@Slf4j
@RequiredArgsConstructor
public class ReaderControllerV4 {

    private final ReaderServiceV4 readerServiceV4;

    @GetMapping("/read-file/der")
    public void readerDer() throws Exception {
        log.info("start read process using der file.");
        readerServiceV4.readFileDer();
    }

    @GetMapping("/read-file/gpg")
    public void readerGpg() {
        log.info("start read process using gpg file.");
    }

}
